git-tool
========

Odoo branch creation tool, allow you to create a feature branch quickly

Get the Source
--------------
Download a pre checkout repository from http://www.openerp.co.in/source/odoo.tar and extract development directory

	$ tar -xf odoo.tar

Your directory structure should be as below 

	odoo
	|
	+— master
	+— master-tooltip-fix (created featured branch based on the master)
	+— saas4-tooltip-fix (created featured branch based on the saas4)
	+— checkout.sh
	+— git-tool


Configure
---------

Change the configuration of git according to you, set your user and email address

	$ cd odoo/odoo
	$ vim .git/config

change below portion, your name and email you use in git.

	 [user]
	 name = Your Name
	 email = code@openerp.com

Create Branch
--------------

Once you configure the working directory you are ready to create a and checkout feature branch

	$ ./git-tool master-feature-branch base-branch

Lets assume that you have to create a new feature that contains the changes for reviewing the tooltip for all account applications

	$ ./git-tool master-account-tooltip-mga

The default brach is dev/master based on that your working copy will be created, if you want to work with other branches like 7.0 or saas4 you have to apply addition argument to git-tool as below

	$ ./git-tool saas4-account-tooltip-fix-mga odoo/saas4
	$ ./git-tool 7.0-account-tooltip-fix-mga odoo/7.0

Branch Name
-----------

Depending on what you are going to develop and fix you have to crete a stack branch based on that, if you going to fix the soem bug in 7.0 you should keep the name of your branch like 7.0-bug-desc-mga. in case if you develop a new feature based on the master name should be master-feature-mga, where replace mga with your employee code. 

Push your Branch
----------------

Push your branch to odoo-dev using the git push command, first time you have create a new feature branch on odoo-dev/odoo you have to execute the below command 

	$  git push -u dev your-branch-name

for created branch on git use the below command

	$  git push dev your-branch-name or $  git push will also work

Happy Developing
